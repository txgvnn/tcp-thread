#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <string.h>

#define PORT 2505

int sended = 0;
long size_file = 16777000;
char name_file[30];
FILE *file;

void welcome();
void* send_file(void* unused){
	struct sockaddr_in server;
	int sockfd;
	char buff[2];
	int result;		// ket qua send tcp
	
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);
	server.sin_addr.s_addr = INADDR_ANY;
	
	if (( sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		perror("socket");
	
	if (connect(sockfd, (struct sockaddr *) &server, sizeof(server))== -1)
		perror("connect");
	
	send(sockfd,name_file, sizeof(name_file),0);
	send(sockfd,&size_file, sizeof(size_file),0);
	
	while(sended < size_file){	
		fread(buff,1,1,file);
		result = send(sockfd,buff,1,0);
		
		if (result <0){
			perror("send");
			break;
		}
		
		if (result > 0){
			sended++;
			continue;
		}		
	}
}

int main(int argc, char* argv[]){
	pthread_t thread_id;
	
	if ( argc != 2){
		fprintf(stderr,"Usage: %s name-file-to-send\n", argv[0]);
		return 1;
	}

	/* File */
	strcpy(name_file,argv[1]);
	file = fopen(name_file,"r");
	if (file == NULL){
		perror("file");
		exit(1);
	}
	
	fseek(file,0, SEEK_END);
	size_file = ftell(file);
	fseek(file,0,SEEK_SET);
	/* File */
	
	welcome();
	fprintf(stdout,"File: %s\nSize: %.2f kB\n",argv[1],(float)size_file/1024);
	
	if (pthread_create(&thread_id, NULL, &send_file,NULL) != 0 )
		fprintf(stderr,"Error create thread\n");
	
	float percent = 0;
	int show = -1;
	int i;
	printf("\n");
	while(1){
		show = (++show)%500000;
		if (show !=0 ) continue;
		fprintf(stdout,"\033[1A\033[2K");		
		percent = (float)sended*100/size_file;
		fprintf(stdout,"Send: %.1f %c\t[", percent,'\x25');
		for ( i = 0; i < (int)percent/2; i++)
			printf("=");
		
		if (i <49) 
			printf(">");
		else 
			printf("=");
		i++;

		while(i<50){
			printf(" ");
			i++;
		}
		
		printf("]\n");
		
		if (sended == size_file){
			fprintf(stdout,"\033[1A\033[2K");		

			fprintf(stdout,"Send: %2d %c\t[", 100,'\x25');
			for ( i = 0; i < 50; i++)
				printf("=");
			printf("]\n");
			fprintf(stdout, "Send file successed!\n");
			break;
		}
	}

	fclose(file);
	exit(0);
}
void welcome(){
	system("echo '\e[0;34m'");
	system("echo '  #####        ###   #   #  #   #  #   # '");
	system("echo '    #         #      #   #  ##  #  ##  # '");
	system("echo '    #    \e[0;31m# #\e[0;34m  # ##   #   #  # # #  # # # '");
	system("echo '    #     \e[0;31m#\e[0;34m   #   #   # #   #  ##  #  ## '");
	system("echo '    #    \e[0;31m# #\e[0;34m   ###     #    #   #  #   # '");
	system("echo ''");
	system("echo '  \e[0;36m[ @author TxGVNN ]\e[0m'");
	system("echo ''");
	printf("Send file version 1.0\n\n");
}
