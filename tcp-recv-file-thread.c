#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>

#define PORT 2505

void welcome();

void* recv_file(void *sock){
	int sock_client = *(int*)sock;
	int size_file;
	int result;
	int buff[32];
	recv(sock_client,buff,32,0);
	printf(">thread[%d]File name: \"%s\"\n", pthread_self(),buff);
	recv(sock_client,buff,sizeof(long),0);
	size_file = *(long*)buff;
	printf(">thread[%d]Size: %ld bytes\n", pthread_self(),size_file);
	int size_revc =0;		
	while (1){
		result = recv(sock_client,buff,1,0);
		if (result == -1){
			fprintf(stderr,"Mat ket noi\n");
			break;
		} 
		if (result > 0){			
			//fprintf(stdout,"client[%d]: %s (%d bytes)\n",socknew, buff,result);
//			fwrite(buff, 1, 1, file);
			size_revc+= result;
			continue;
		}
		if (result == 0 ){
			break;
		}
	
	}
	if ( size_file == size_revc){
		fprintf(stdout, ">thread[%d]Successed %d bytes!\n",pthread_self(),size_revc);
	} else {
		fprintf(stderr, ">thread[%d]Error tranfer file!\n",pthread_self());
	}	
}
int main(int argc, char* argv[]){
	pthread_t pthread_id[10];
	int sockfd,socknew;
	struct sockaddr_in server,client;
	char *hostname = (char*)malloc(18);
//	memset(&server,0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons(PORT);
	server.sin_addr.s_addr = INADDR_ANY;
	
	welcome();
//	FILE *file;
//	file = fopen("haha.exe","w+");
	
	if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
		perror("socket");
	}
	
	if (bind(sockfd, (struct sockaddr *)&server, sizeof(struct sockaddr)) == -1){
		perror("bind");
	}
	
	if (listen(sockfd, 10) == -1 ){
		perror("listen");
	}
	
	int len = sizeof(client);
	int i=0;
	while (1){
		fprintf(stdout, "\nServer listen in %d\n", PORT);	
		socknew = accept(sockfd, (struct sockaddr *)&client, (socklen_t *)&len);

		if (socknew<0){
			perror("accept");
			continue;
		}
		
		if (pthread_create(&pthread_id[i], NULL, &recv_file,&socknew) != 0 )
		fprintf(stderr,"Error create thread\n");
		inet_ntop(client.sin_family,&(client.sin_addr),hostname,18);
		printf("Got connect from %s:%d\n", hostname, client.sin_port);
		i++;

	}
	//fclose(file);
}
void welcome(){
	system("echo '\e[0;34m'");
	system("echo '  #####        ###   #   #  #   #  #   # '");
	system("echo '    #         #      #   #  ##  #  ##  # '");
	system("echo '    #    \e[0;31m# #\e[0;34m  # ##   #   #  # # #  # # # '");
	system("echo '    #     \e[0;31m#\e[0;34m   #   #   # #   #  ##  #  ## '");
	system("echo '    #    \e[0;31m# #\e[0;34m   ###     #    #   #  #   # '");
	system("echo ''");
	system("echo '  \e[0;36m[ @author TxGVNN ]\e[0m'");
	system("echo ''");
}